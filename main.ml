open Lexer
open Parser
open ASD

let () =
  (* Use with a manually made AST 
  let ast = (Doc [Triplet (Sujet (Ent "Maïs"), [Double ((Pred (Ent "Couleur")), [Str "Jaune"]); Double ((Pred (Ent "Saison")), [Obj (Ent "Été"); Obj (Ent "Automne")])]); Triplet (Sujet (Ent "Aubergine"), [Double ((Pred (Ent "Couleur")), [Str "Violet"])])])
  in ASD.ntriples_of_ast ast
*)
  (* Use with lexer and parser *)
  let f = open_in Sys.argv.(1) in
  let lexbuf = Lexing.from_channel f
  in try
    let token_stream = Stream.of_list (Lexer.tokenize lexbuf)
    in let ast = Parser.parse token_stream
    in ASD.ntriples_of_ast ast
  with Lexer.Unexpected_character e ->
  begin
    Printf.printf
      "Unexpected character: `%c' at position '%d' on line '%d'\n"
      e.character e.pos e.line;
    exit 1
  end
  

