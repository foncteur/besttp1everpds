open ASD
open Token

let parse_objet = parser
	| [< 'ENTITY s >] -> Obj(Ent(s))
	| [< 'STRING s >] -> Str(s) ;;

let rec parse_objet_liste = parser
	| [< 'TOKENCOLON ; objet1 = parse_objet ; suite = parse_objet_liste >] -> objet1::suite
	| [< >] -> []
	| [< EOF >] -> [] ;;

let parse_doublet = parser
	| [< 'ENTITY s ; premiertruc = parse_objet ; suite = parse_objet_liste >] -> Double(Pred(Ent(s)), premiertruc::suite);;

let rec parse_doublet_liste = parser 
	| [< 'TOKENSEMICOLON ; double1 = parse_doublet ; suite = parse_doublet_liste >] -> double1::suite
	| [< >] -> []
	| [< EOF >] -> [] ;;

let parse_triplet = parser
	| [< 'ENTITY s ; premierdoublet = parse_doublet ; doublet_liste = parse_doublet_liste >] -> Triplet(Sujet(Ent(s)), premierdoublet::doublet_liste);;

let rec parse_triplet_list = parser
	| [< tripl = parse_triplet ; 'TOKENPOINT ; tail = parse_triplet_list >] -> tripl::tail
	| [< >] -> []
	| [< EOF >] -> [] ;;

let parse (tokens:token Stream.t) = 
	Doc(parse_triplet_list(tokens));;

