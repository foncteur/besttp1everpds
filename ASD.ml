(* ASD type *)

type entity = Ent of string;;
type obj = Obj of entity | Str of string;;
type pred = Pred of entity;;
type double = Double of pred * (obj list);;
type sujet = Sujet of entity;;
type triplet = Triplet of sujet * (double list);;
type document = Doc of triplet list;;

let ntriples_of_ent e =
    let Ent s = e in print_char '<'; print_string s; print_char '>';;

let ntriples_of_str st = print_char '"'; print_string st; print_char '"';;

let ntriples_of_obj o =
    match o with
    |Obj e -> ntriples_of_ent e;
    |Str s -> ntriples_of_str s;;
    
let ntriples_of_pre p =
    let Pred e = p in ntriples_of_ent e;;

let ntriples_of_double d =
    let Double (p, ol) = d in ntriples_of_pre p; print_char ' '; let h::q = ol in ntriples_of_obj h;;

let ntriples_of_suj s =
    let Sujet e = s in ntriples_of_ent e;;

let rec ntriples_of_tri t =
    let Triplet (s, dl) = t in 
    match dl with
        | [] -> ();
        | d::q -> let Double (p, ol) = d 
                  in match ol with 
                     | [] -> ntriples_of_tri (Triplet (s, q));
                     | o::q' -> ntriples_of_suj s; print_char ' '; ntriples_of_double d; print_string " .\n"; ntriples_of_tri (Triplet (s, ((Double (p, q'))::q)));;



(* Function to generate the document out of the AST *)
let rec ntriples_of_ast ast = 
    let Doc tl = ast in match tl with
            | [] -> ();
            | t::q -> ntriples_of_tri t; ntriples_of_ast (Doc q);;
            
